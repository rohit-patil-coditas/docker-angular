FROM node:16.17.1-alpine3.16 AS builder
COPY simple-angular6-app/src/app
WORKDIR /app
RUN npm install --save --legacy-peer-deps
RUN npm install -g @angular/cli
RUN npm run build --prod

## Stage -2 ##
FROM httpd:alpine
COPY --from=builder /app/dist/simple-angular6-app/ /usr/share/nginx/html
